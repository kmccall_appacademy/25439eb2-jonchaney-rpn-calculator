class RPNCalculator
  attr_reader :value

  def initialize
    @array = []
    @value = 0
  end

  def push(num)
    @array.push(num)
  end

  def plus
    @array = eval(:+)
  end

  def minus
    @array = eval(:-)
  end

  def times
    @array = eval(:*)
  end

  def divide
    @array = eval(:/)
  end

  def tokens(string)
    string.delete(" ").chars.map do |el|
      if symbol(el) == nil
        el.to_i
      else
        symbol(el)
      end
    end
  end

  def evaluate(string)
    tokens(string).each do |el|
      if el.class == Integer
        @array << el
      else
        @array = eval(el)
      end
    end
    @value
  end

  private

  def eval(sym)
    if @array.empty?
      raise "calculator is empty"
    end
    equation = []
    equation << @array.pop.to_f
    equation << @array.pop
    @value = equation.reverse.reduce(sym)
    @array.push(@value)
  end

  def symbol(char)
    {"+" => :+, "-" => :-, "*" => :*, "/" => :/}[char]
  end

end
